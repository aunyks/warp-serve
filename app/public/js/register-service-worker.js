if ('serviceWorker' in navigator) {
  try {
    // Register
    await navigator.serviceWorker.register('/js/service-worker.js', {
      scope: '/',
    })
    console.log('Service worker registered')
  } catch (error) {
    console.error('Service worker not registered', error)
  }
}
