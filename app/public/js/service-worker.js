// Name of our cache for reference in-browser
const cacheName = 'warpwa-cache'
// Assets to download to cache on install
const staticAssets = ['/']

self.addEventListener('install', async (e) => {
  const cache = await caches.open(cacheName)
  await cache.addAll(staticAssets)
  return self.skipWaiting()
})

self.addEventListener('activate', (e) => {
  self.clients.claim()
})

self.addEventListener('push', (event) => {
  const notiDetails = event.data.json()
  event.waitUntil(
    self.registration.showNotification(notiDetails.title, {
      ...notiDetails,
    })
  )
})

self.addEventListener('notificationclick', (event) => {
  // This looks to see if the target URL is already open and
  // focuses if it is, else it opens it
  const targetPath = '/'
  event.waitUntil(
    clients
      .matchAll({
        type: 'window',
      })
      .then((clientList) => {
        for (let i = 0; i < clientList.length; i++) {
          const client = clientList[i]
          if (
            new URL(client.url).pathname === targetPath &&
            'focus' in client
          ) {
            return client.focus()
          }
        }
        if (clients.openWindow) {
          return clients.openWindow(targetPath)
        }
      })
  )

  // Android doesn’t close the notification when you click on it
  // See: http://crbug.com/463146
  event.notification.close()
})

self.addEventListener('fetch', async (e) => {
  const req = e.request
  const url = new URL(req.url)
  // Ignore certain media types and let their
  // requests pass through
  if (
    url.pathname.endsWith('.mp4') ||
    url.pathname.endsWith('.mp3') ||
    url.pathname.endsWith('.wav') ||
    url.pathname.endsWith('.ogg')
  ) {
    return
  }
  e.respondWith(networkAndCache(req))
})

// Use this to respond to requests from the network
// first and fall back to cache if not available
async function networkAndCache(req) {
  const cache = await caches.open(cacheName)
  try {
    const fresh = await fetch(req)
    // If we're fetching something useful (not a Chrome extension),
    // let's cache it
    const urlProtocol = new URL(req.url).protocol
    const shouldCacheResponse =
      req.method === 'GET' && urlProtocol.startsWith('http')
    if (shouldCacheResponse) {
      await cache.put(req, fresh.clone())
    }
    return fresh
  } catch (e) {
    const cached = await cache.match(req)
    return cached
  }
}
