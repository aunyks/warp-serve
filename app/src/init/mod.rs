use crate::init::mail::SmtpConfig;
use crate::routes::*;
use crate::templates::Template;
use diesel::r2d2::{ConnectionManager, Pool};
use diesel::{Connection, PgConnection};
use diesel_migrations::{embed_migrations, EmbeddedMigrations, MigrationHarness};
use handlebars::Handlebars;
use std::{env, error::Error, fs::File, sync::Arc, time::Duration};
use warp::{Filter, Rejection, Reply};

pub mod mail;

const DB_MIGRATIONS: EmbeddedMigrations = embed_migrations!("../db/migrations");

pub fn connect_and_migrate_to_db(connection_string: &str) -> Pool<ConnectionManager<PgConnection>> {
    let sql_conn_timeout = env::var("SQL_CONN_TIMEOUT")
        .expect("No SQL_CONN_TIMEOUT provided!")
        .parse()
        .unwrap_or(3);

    let mut pg_conn =
        PgConnection::establish(&connection_string).expect("Error connecting to database");
    pg_conn
        .run_pending_migrations(DB_MIGRATIONS)
        .expect("Couldn't migrate DB");

    let conn_manager = ConnectionManager::<PgConnection>::new(connection_string);
    Pool::builder()
        .connection_timeout(Duration::from_secs(sql_conn_timeout))
        .build(conn_manager)
        .expect("Couldn't build DB connection pool")
}

pub fn load_handlebars_templates(hb: &mut Handlebars) -> Result<(), Box<dyn Error>> {
    hb.register_template_file(Template::Homepage.into(), "app/src/templates/homepage.hbs")?;
    hb.register_template_file(Template::Signup.into(), "app/src/templates/auth/signup.hbs")?;
    hb.register_template_file(Template::Login.into(), "app/src/templates/auth/login.hbs")?;
    hb.register_template_file(
        Template::ChangePassword.into(),
        "app/src/templates/auth/change-password.hbs",
    )?;

    hb.register_partial(
        "global-html-head",
        include_str!("../templates/partials/global-html-head.hbs"),
    )?;

    Ok(())
}

pub struct RouteDependencies<'a> {
    pub hb: Arc<Handlebars<'a>>,
    pub db_conn: Arc<Pool<ConnectionManager<PgConnection>>>,
    pub vapid_key_file: Arc<File>,
    pub smtp_config: Arc<SmtpConfig>,
}

pub fn routes<'a>(
    deps: RouteDependencies,
) -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone + '_ {
    homepage(deps.hb.clone())
        .or(auth_routes(&deps))
        .or(notis::send_notification(deps.vapid_key_file))
        .or(static_files())
        .map(|reply| warp::reply::with_header(reply, "X-Scripture", "Psalm 34:1"))
}

pub fn auth_routes<'a>(
    deps: &RouteDependencies<'a>,
) -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone + 'a {
    auth::signup_page(deps.hb.clone())
        .or(auth::signup_endpoint(
            deps.hb.clone(),
            deps.db_conn.clone(),
            deps.smtp_config.clone(),
        ))
        .or(auth::login_page(deps.hb.clone()))
        .or(auth::change_password_page(deps.hb.clone()))
        .or(auth::login_endpoint(deps.hb.clone(), deps.db_conn.clone()))
        .or(auth::verify_email_endpoint(deps.db_conn.clone()))
        .or(auth::change_password_endpoint(deps.db_conn.clone()))
        .or(auth::logout(deps.db_conn.clone()))
}
