use std::env;
use std::str::FromStr;
use std::time::Duration;

use lettre::{
    transport::smtp::{authentication::Credentials, Error, PoolConfig, SmtpTransportBuilder},
    SmtpTransport,
};

pub enum TransportSecurity {
    None,
    Tls,
    StartTls,
}

impl FromStr for TransportSecurity {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "none" => Ok(Self::None),
            "tls" => Ok(Self::Tls),
            "starttls" => Ok(Self::StartTls),
            _ => Err(()),
        }
    }
}

pub struct SmtpConfig {
    pub host: String,
    pub port: u16,
    pub pool_size: u32,
    pub username: String,
    pub password: String,
    pub timeout: u64,
    pub transport_security: TransportSecurity,
}

impl Default for SmtpConfig {
    fn default() -> Self {
        Self {
            host: env::var("SMTP_HOST").expect("No SMTP_HOST provided!"),
            port: env::var("SMTP_PORT")
                .expect("No SMTP_PORT provided!")
                .parse()
                .expect("Couldn't parse SMTP_PORT to a number"),
            pool_size: env::var("SMTP_POOL_SIZE")
                .expect("No SMTP_POOL_SIZE provided!")
                .parse()
                .expect("Couldn't parse SMTP_POOL_SIZE to a number"),
            username: env::var("SMTP_USERNAME").expect("No SMTP_USERNAME provided!"),
            password: env::var("SMTP_PASSWORD").expect("No SMTP_PASSWORD provided!"),
            timeout: env::var("SMTP_TIMEOUT")
                .expect("No SMTP_TIMEOUT provided!")
                .parse()
                .expect("Couldn't parse SMTP_TIMEOUT to a number"),
            transport_security: env::var("SMTP_TRANSPORT_SECURITY")
                .expect("No SMTP_TRANSPORT_SECURITY provided!")
                .parse()
                .expect("Couldn't parse SMTP_TRANSPORT_SECURITY to a number"),
        }
    }
}

impl SmtpConfig {
    pub fn init() -> Self {
        Self::default()
    }

    pub fn get_transport(&self) -> SmtpTransport {
        let transport_type: Result<SmtpTransportBuilder, Error> = match self.transport_security {
            TransportSecurity::None => Ok(SmtpTransport::builder_dangerous(&self.host)),
            TransportSecurity::Tls => SmtpTransport::relay(&self.host),
            TransportSecurity::StartTls => SmtpTransport::starttls_relay(&self.host),
        };

        transport_type
            .expect("Couldn't build SMTP transport from SMTP_HOST")
            .port(self.port)
            .timeout(Some(Duration::from_millis(self.timeout)))
            .credentials(Credentials::new(
                self.username.clone(),
                self.password.clone(),
            ))
            .pool_config(PoolConfig::new().max_size(self.pool_size))
            .build()
    }
}
