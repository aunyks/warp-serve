pub enum Template {
    Homepage,
    Signup,
    Login,
    ChangePassword,
}

impl From<Template> for &str {
    fn from(value: Template) -> Self {
        match value {
            Template::Homepage => "homepage",
            Template::Signup => "signup",
            Template::Login => "login",
            Template::ChangePassword => "change-password",
        }
    }
}
