use db::models::*;
use crate::{filters::auth::*, init::mail::*, templates::Template, meta};
use argon2::{
    password_hash::{rand_core::OsRng, PasswordHash, PasswordHasher, PasswordVerifier, SaltString},
    Argon2,
};
use cookie::{time::Duration, Cookie};
use diesel::r2d2::PooledConnection;
use diesel::{
    prelude::*,
    r2d2::{ConnectionManager, Pool},
};
use handlebars::Handlebars;
use lettre::message::header::ContentType;
use lettre::{Message, Transport};
use serde::{Deserialize, Serialize};
use serde_json::json;
use std::{string::String, sync::Arc};
use uuid::Uuid;
use warp::{hyper::StatusCode, Filter, Rejection, Reply};

#[derive(Serialize, Deserialize)]
struct ResourceCode {
    code: String,
}

#[derive(Serialize, Deserialize)]
struct PasswordChangeDetails {
    code: String,
    new_password: String,
}

/// Creates a new login session for a user with provided ID and
/// returns the login token associated with the session.
fn create_login_session(
    user_id: i64,
    conn: &mut PooledConnection<ConnectionManager<PgConnection>>,
) -> String {
    let login_token = Uuid::new_v4().to_string();
    let _ = diesel::insert_into(db::schema::login_sessions::table)
        .values(NewLoginSession {
            user_id,
            token: login_token.clone(),
        })
        .execute(conn)
        .expect("Could not create login session");
    login_token
}

fn hash_password(plaintext: String) -> String {
    let salt = SaltString::generate(&mut OsRng);

    let argon2 = Argon2::default();
    argon2
        .hash_password(plaintext.as_bytes(), &salt)
        .expect("Couldn't hash password")
        .to_string()
}

fn verify_password(plaintext: String, hashed: String) -> bool {
    let parsed_hash = PasswordHash::new(&hashed).expect("Invalid password hash");
    Argon2::default()
        .verify_password(plaintext.as_bytes(), &parsed_hash)
        .is_ok()
}

/// `GET /signup`
///
/// The signup page.
pub fn signup_page(
    hb: Arc<Handlebars<'_>>,
) -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone + '_ {
    warp::path("signup")
        .and(warp::path::end())
        .and(warp::get())
        .map(move || warp::reply::html(hb.clone().render(Template::Signup.into(), &()).unwrap()))
}

/// `GET /login`
///
/// The login page.
pub fn login_page(
    hb: Arc<Handlebars<'_>>,
) -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone + '_ {
    warp::path("login")
        .and(warp::path::end())
        .and(warp::get())
        .map(move || warp::reply::html(hb.clone().render(Template::Login.into(), &()).unwrap()))
}

/// `GET /change-password`
///
/// The change password page.
pub fn change_password_page(
    hb: Arc<Handlebars<'_>>,
) -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone + '_ {
    warp::path("change-password")
        .and(warp::path::end())
        .and(warp::get())
        .map(move || {
            warp::reply::html(
                hb.clone()
                    .render(Template::ChangePassword.into(), &())
                    .unwrap(),
            )
        })
}

/// `POST /signup`
///
/// Creates a new user and login session for a user with valid username, email, and password.
pub fn signup_endpoint(
    hb: Arc<Handlebars<'_>>,
    db_pool: Arc<Pool<ConnectionManager<PgConnection>>>,
    smtp_config: Arc<SmtpConfig>,
) -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone + '_ {
    warp::path("signup")
        .and(warp::path::end())
        .and(warp::post())
        .and(warp::body::form())
        .and_then(crate::filters::auth::signup)
        .and_then(move |signup_details: SignupDetails| {
            let hb = hb.clone();
            let smtp_config = smtp_config.clone();
            let db_conn = db_pool.get();
            async move {
                let mut err_msg: Option<String> = None;

                if let Ok(mut db_conn) = db_conn {
                    let hashed_password = hash_password(signup_details.password);

                    let tx_result: Result<(User, Email), diesel::result::Error> =
                        db_conn.build_transaction().run(|tx_conn| {
                            let new_user = diesel::insert_into(db::schema::users::table)
                                .values(NewUser {
                                    username: signup_details.username.clone(),
                                    password: hashed_password.clone(),
                                })
                                .returning(User::as_returning())
                                .get_result(tx_conn)?;

                            let new_email = diesel::insert_into(db::schema::emails::table)
                                .values(NewEmail {
                                    user_id: new_user.id,
                                    email: signup_details.email.clone(),
                                    is_primary: true,
                                })
                                .returning(Email::as_returning())
                                .get_result(tx_conn)?;

                            Ok((new_user, new_email))
                        });

                    match tx_result {
                        Err(tx_err) => match tx_err {
                            diesel::result::Error::DatabaseError(err_kind, err_details) => {
                                match err_kind {
                                    diesel::result::DatabaseErrorKind::UniqueViolation => {
                                        if err_details.message().contains("users_username_key") {
                                            err_msg = Some(String::from("Username already exists"));
                                        }
                                        if err_details.message().contains("emails_email_key") {
                                            err_msg = Some(String::from("Email already exists"));
                                        }
                                    }
                                    _ => {
                                        err_msg = Some(String::from(
                                            "An unknown error occured. Try again in a few minutes",
                                        ));
                                    }
                                }
                            }
                            _ => {
                                err_msg = Some(String::from(
                                    "An unknown error occured. Try again in a few minutes",
                                ));
                            }
                        },
                        Ok((new_user, new_email)) => {
                            let login_token = create_login_session(new_user.id, &mut db_conn);

                            let email_verification_request = diesel::insert_into(
                                db::schema::email_verification_requests::table,
                            )
                            .values(NewEmailVerificationRequest {
                                email_id: new_email.id,
                                code: Uuid::new_v4().to_string(),
                            })
                            .returning(EmailVerificationRequest::as_returning())
                            .get_result(&mut db_conn);

                            tokio::task::spawn(async move {
                                if let Ok(email_verification_request) = email_verification_request {
                                    let email_verification_msg = Message::builder()
                                    .from(format!("WarpWA <no-reply@{}>", meta::app_domain()).parse().unwrap())
                                    .to(format!("New User <{}>", new_email.email).parse().unwrap())
                                    .subject("Verify your email")
                                    .header(ContentType::TEXT_PLAIN)
                                    .body(format!("Visit the following link to verify your email:\n\n{}://{}/verify-email?code={}", meta::app_protocol(), meta::app_domain(), email_verification_request.code))
                                    .expect("Couldn't build email message");

                                    let mailer = smtp_config.get_transport();
                                    
                                    mailer.send(&email_verification_msg)
                                        .expect("Couldn't send email verification email!");
                                }
                            });

                            return Ok::<warp::http::Response<String>, Rejection>(
                                warp::http::Response::builder()
                                    .status(StatusCode::SEE_OTHER)
                                    .header("Location", format!("{}://{}/dashboard", meta::app_protocol(), meta::app_domain()))
                                    .header(
                                        "Set-Cookie",
                                        Cookie::build("login_token", login_token)
                                            .secure(true)
                                            .http_only(true)
                                            .finish()
                                            .encoded()
                                            .to_string(),
                                    )
                                    .body("".into())
                                    .expect("Couldn't create response builder"),
                            );
                        }
                    }
                } else {
                    err_msg = Some(String::from(
                        "An unknown error occured. Try again in a few minutes",
                    ))
                }

                Ok::<warp::http::Response<String>, Rejection>(
                    warp::http::Response::builder()
                        .status(StatusCode::OK)
                        .header("content-type", "text/html; charset=utf-8")
                        .body(
                            hb.render(Template::Signup.into(), &json!({"error": err_msg}))
                                .unwrap(),
                        )
                        .expect("Couldn't create response builder"),
                )
            }
        })
}

/// `POST /login`
///
/// Creates a new login session for a user with valid (username or email) and password.
pub fn login_endpoint(
    hb: Arc<Handlebars<'_>>,
    db_pool: Arc<Pool<ConnectionManager<PgConnection>>>,
) -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone + '_ {
    warp::path("login")
        .and(warp::path::end())
        .and(warp::post())
        .and(warp::body::form())
        .and_then(crate::filters::auth::login)
        .and_then(move |login_details: LoginDetails| {
            let hb = hb.clone();
            let db_conn = db_pool.get();
            async move {
                let err_msg: Option<String>;

                if let Ok(mut db_conn) = db_conn {
                    let user_fetch: Result<User, diesel::result::Error> =
                        if let Some(username) = login_details.username {
                            db::schema::users::table
                                .filter(db::schema::users::username.eq(&username))
                                .select(User::as_select())
                                .get_result(&mut db_conn)
                        } else {
                            if let Some(email) = login_details.email {
                                db::schema::emails::table
                                    .filter(db::schema::emails::email.eq(&email))
                                    .select(Email::as_select())
                                    .get_result(&mut db_conn)
                                    .and_then(|email| {
                                        db::schema::users::table
                                            .filter(db::schema::users::id.eq(&email.user_id))
                                            .select(User::as_select())
                                            .get_result(&mut db_conn)
                                    })
                            } else {
                                // The login filter will guarantee either a username or email if we've
                                // made it to this filter.
                                unreachable!();
                            }
                        };

                    match user_fetch {
                        Err(query_err) => match query_err {
                            diesel::result::Error::NotFound => {
                                err_msg = Some(String::from("User not found"));
                            }
                            _ => {
                                err_msg = Some(String::from(
                                    "An unknown error occured. Try again in a few minutes",
                                ));
                            }
                        },
                        Ok(user) => {
                            if verify_password(login_details.password, user.password) {
                                let login_token = create_login_session(user.id, &mut db_conn);

                                return Ok::<warp::http::Response<String>, Rejection>(
                                    warp::http::Response::builder()
                                        .status(StatusCode::SEE_OTHER)
                                        .header("Location", format!("{}://{}/dashboard", meta::app_protocol(), meta::app_domain()))
                                        .header(
                                            "Set-Cookie",
                                            Cookie::build("login_token", login_token)
                                                .secure(true)
                                                .http_only(true)
                                                .finish()
                                                .encoded()
                                                .to_string(),
                                        )
                                        .body("".into())
                                        .expect("Couldn't create response builder"),
                                );
                            } else {
                                err_msg = Some(String::from("Password incorrect"));
                            }
                        }
                    }
                } else {
                    err_msg = Some(String::from(
                        "An unknown error occured. Try again in a few minutes",
                    ))
                }

                Ok::<warp::http::Response<String>, Rejection>(
                    warp::http::Response::builder()
                        .status(StatusCode::OK)
                        .header("content-type", "text/html; charset=utf-8")
                        .body(
                            hb.render(Template::Login.into(), &json!({"error": err_msg}))
                                .unwrap(),
                        )
                        .expect("Couldn't create response builder"),
                )
            }
        })
}

/// `GET /logout`
///
/// Deletes the user's login token if it exists in the `login_sessions` table.
/// Schedules the `login_token` cookie for deletion.
pub fn logout(
    db_pool: Arc<Pool<ConnectionManager<PgConnection>>>,
) -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    warp::path("logout")
        .and(warp::path::end())
        .and(warp::get())
        .and(warp::cookie("login_token"))
        .map(move |login_token: String| {
            let db_conn = db_pool.get();

            let mut res = warp::http::Response::builder()
                .status(StatusCode::SEE_OTHER)
                .header("Location", format!("{}://{}/", meta::app_protocol(), meta::app_domain()));

            if let Ok(mut db_conn) = db_conn {
                let _delete_session = diesel::delete(
                    db::schema::login_sessions::table
                        .filter(db::schema::login_sessions::token.eq(&login_token)),
                    )
                    .execute(&mut db_conn);

                res = res.header(
                    "Set-Cookie",
                    Cookie::build("login_token", "goodbye")
                        .secure(true)
                        .http_only(true)
                        .max_age(Duration::SECOND)
                        .finish()
                        .encoded()
                        .to_string(),
                );
            }

            res.body("".to_string()).expect("Couldn't build response")
        })
}

/// `GET /verify-email`
///
/// Sets the user's email to verified and deletes the verification request.
pub fn verify_email_endpoint(
    db_pool: Arc<Pool<ConnectionManager<PgConnection>>>,
) -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    warp::path("verify-email")
        .and(warp::path::end())
        .and(warp::get())
        .and(warp::query())
        .and_then(move |resource_code: ResourceCode| {
            let db_conn = db_pool.get();

            async move {
                if let Ok(mut db_conn) = db_conn {
                    let _email_verification = db::schema::email_verification_requests::table
                        .filter(
                            db::schema::email_verification_requests::code
                                .eq(&resource_code.code),
                        )
                        .select(EmailVerificationRequest::as_select())
                        .get_result(&mut db_conn)
                        .and_then(|veri_request| {
                            // Get email corresponding to the verification request
                            db::schema::emails::table
                                .filter(db::schema::emails::id.eq(veri_request.email_id))
                                .select(Email::as_select())
                                .get_result(&mut db_conn)
                        })
                        .and_then(|email| {
                            // Set the email as verified
                            diesel::update(db::schema::emails::table)
                                .filter(db::schema::emails::id.eq(email.id))
                                .set(db::schema::emails::is_verified.eq(true))
                                .execute(&mut db_conn)
                        })
                        .and_then(|_| {
                            // Delete the verification request
                            diesel::delete(
                                db::schema::email_verification_requests::table.filter(
                                    db::schema::email_verification_requests::code
                                        .eq(&resource_code.code),
                                ),
                            )
                            .execute(&mut db_conn)
                        });
                }

                Ok::<warp::http::Response<String>, Rejection>(
                    warp::http::Response::builder()
                        .status(StatusCode::SEE_OTHER)
                        .header("Location", format!("{}://{}/", meta::app_protocol(), meta::app_domain()))
                        .body("".to_string())
                        .expect("Couldn't build response"),
                )
            }
        })
}

/// `POST /change-password`
///
/// Updates the user's password and deletes the password change request.
pub fn change_password_endpoint(
    db_pool: Arc<Pool<ConnectionManager<PgConnection>>>,
) -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    warp::path("change-password")
        .and(warp::path::end())
        .and(warp::post())
        .and(warp::body::form())
        .and_then(move |change_details: PasswordChangeDetails| {
            let db_conn = db_pool.get();

            async move {
                if let Ok(mut db_conn) = db_conn {
                    let _email_verification = db::schema::password_change_requests::table
                        .filter(
                            db::schema::password_change_requests::code.eq(&change_details.code),
                        )
                        .select(PasswordChangeRequest::as_select())
                        .get_result(&mut db_conn)
                        .and_then(|change_request| {
                            // Get user corresponding to the change request
                            db::schema::users::table
                                .filter(db::schema::users::id.eq(&change_request.user_id))
                                .select(User::as_select())
                                .get_result(&mut db_conn)
                        })
                        .and_then(|user| {
                            // Set the password
                            let hashed_password = hash_password(change_details.new_password.clone());

                            diesel::update(db::schema::users::table)
                                .filter(db::schema::users::id.eq(user.id))
                                .set(db::schema::users::password.eq(hashed_password))
                                .execute(&mut db_conn)
                        })
                        .and_then(|_| {
                            // Delete the verification request
                            diesel::delete(
                                db::schema::password_change_requests::table.filter(
                                    db::schema::password_change_requests::code
                                        .eq(&change_details.code),
                                ),
                            )
                            .execute(&mut db_conn)
                        });
                }

                Ok::<warp::http::Response<String>, Rejection>(
                    warp::http::Response::builder()
                        .status(StatusCode::SEE_OTHER)
                        .header("Location", format!("{}://{}/login", meta::app_protocol(), meta::app_domain()))
                        .body("".to_string())
                        .expect("Couldn't build response"),
                )
            }
        })
}
