use crate::templates::Template;
use handlebars::Handlebars;
use std::sync::Arc;
use warp::{Filter, Rejection, Reply};

pub mod auth;
pub mod notis;

/// `GET /public/**/*`
/// Static assets
pub fn static_files() -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    warp::fs::dir("app/public/")
        .and(warp::get())
        .map(|public_file: warp::filters::fs::File| {
            if public_file.path().ends_with("service-worker.js") {
                warp::reply::with_header(public_file, "Service-Worker-Allowed", "/").into_response()
            } else {
                public_file.into_response()
            }
        })
}

/// `GET /`
/// The homepage.
pub fn homepage(
    hb: Arc<Handlebars<'_>>,
) -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone + '_ {
    warp::path::end().and(warp::get()).map(move || {
        warp::reply::html(
            hb.render(Template::Homepage.into(), &serde_json::json!({"hi": 22}))
                .unwrap(),
        )
    })
}
