use crate::filters::notis::extract_vapid_key_file;
use serde::{Deserialize, Serialize};
use std::{fs::File, io::Seek, io::SeekFrom, sync::Arc};
use thiserror::Error;
use warp::{hyper::StatusCode, reject::Reject, Filter, Rejection, Reply};
use web_push::{
    ContentEncoding, SubscriptionInfo, VapidSignatureBuilder, WebPushClient, WebPushError,
    WebPushMessageBuilder,
};

#[derive(Serialize, Deserialize)]
pub struct PushSubscriptionKeys {
    pub p256dh: String,
    pub auth: String,
}

#[derive(Serialize, Deserialize)]
pub struct PushSubscription {
    pub endpoint: String,
    pub keys: PushSubscriptionKeys,
}

#[derive(Serialize, Deserialize)]
pub struct NotificationDetails {
    pub subscription: PushSubscription,
    pub ttl: u32,
}

#[derive(Serialize, Deserialize)]
pub struct PushNotification {
    pub title: String,
    pub body: String,
    pub silent: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub icon: Option<String>,
    pub dir: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub lang: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub tag: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub timestamp: Option<u32>,
}

impl Default for PushNotification {
    fn default() -> Self {
        // https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerRegistration/showNotification
        Self {
            title: String::from(""),
            body: String::from(""),
            silent: false,
            icon: None,
            dir: String::from("ltr"),
            lang: None,
            tag: None,
            timestamp: None,
        }
    }
}

#[derive(Error, Debug)]
pub enum SendNotiError {
    #[error("Couldn't load signature builder")]
    SignatureLoad(#[source] WebPushError),
    #[error("Couldn't build VapidSignature")]
    SignatureBuild(#[source] WebPushError),
    #[error("Couldn't seek VAPID key file back to start")]
    KeyFileReset(#[source] std::io::Error),
    #[error("Couldn't create push message builder")]
    InitMessageBuilder(#[source] WebPushError),
    #[error("Couldn't build raw notification")]
    NotiSerialization(#[source] serde_json::Error),
    #[error("Couldn't build message builder")]
    BuildMessage(#[source] WebPushError),
    #[error("Couldn't send push message!")]
    MessageSend(#[source] WebPushError),
}
impl Reject for SendNotiError {}

mod handlers {

    use super::*;

    pub async fn send_notification(
        noti_details: NotificationDetails,
        vapid_key_file: Arc<File>,
    ) -> Result<impl Reply, Rejection> {
        let subscription_info = SubscriptionInfo::new(
            noti_details.subscription.endpoint,
            noti_details.subscription.keys.p256dh,
            noti_details.subscription.keys.auth,
        );

        let mut key_file_ref = vapid_key_file.as_ref();

        let signature = VapidSignatureBuilder::from_pem(key_file_ref, &subscription_info)
            .map_err(|load_err| SendNotiError::SignatureLoad(load_err))?
            .build()
            .map_err(|build_err| SendNotiError::SignatureBuild(build_err))?;

        key_file_ref
            .seek(SeekFrom::Start(0))
            .map_err(|seek_err| SendNotiError::KeyFileReset(seek_err))?;

        let mut message_builder = WebPushMessageBuilder::new(&subscription_info)
            .map_err(|init_err| SendNotiError::InitMessageBuilder(init_err))?;

        let notification = serde_json::to_string(&PushNotification {
            title: String::from("This is a title"),
            body: String::from("This is a body"),
            tag: Some(String::from("tag yooo")),
            ..Default::default()
        })
        .map_err(|noti_ser_err| SendNotiError::NotiSerialization(noti_ser_err))?;
        message_builder.set_payload(ContentEncoding::Aes128Gcm, notification.as_bytes());
        message_builder.set_vapid_signature(signature);

        let push_client = WebPushClient::new().expect("Couldn't create push client");

        push_client
            .send(
                message_builder
                    .build()
                    .map_err(|build_err| SendNotiError::BuildMessage(build_err))?,
            )
            .await
            .map_err(|send_err| SendNotiError::MessageSend(send_err))?;

        Ok(StatusCode::OK)
    }
}

pub fn send_notification(
    vapid_key_file: Arc<File>,
) -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    async fn error_recovery(rejection: Rejection) -> Result<impl Reply, Rejection> {
        match rejection.find::<SendNotiError>() {
            None => Err(rejection),
            Some(send_noti_err) => Ok(match send_noti_err {
                SendNotiError::MessageSend(_) => StatusCode::BAD_GATEWAY,
                SendNotiError::NotiSerialization(_) => StatusCode::BAD_REQUEST,
                _ => StatusCode::INTERNAL_SERVER_ERROR,
            }),
        }
    }

    warp::path("send-noti")
        .and(warp::path::end())
        .and(warp::post())
        .and(warp::body::json())
        .and(extract_vapid_key_file(vapid_key_file))
        .and_then(handlers::send_notification)
        .recover(error_recovery)
}
