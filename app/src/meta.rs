use std::env;

pub fn app_domain() -> String {
    env::var("APP_DOMAIN").unwrap_or("localhost".to_owned())
}

pub fn app_protocol() -> String {
    env::var("APP_PROTOCOL").unwrap_or("http".to_owned())
}
