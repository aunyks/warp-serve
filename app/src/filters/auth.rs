use db::models::*;
use diesel::{
    prelude::*,
    r2d2::{ConnectionManager, Pool},
};
use regex::Regex;
use std::{collections::HashMap, sync::Arc};
use std::{convert::Infallible, fmt::Display};
use thiserror::Error;
use warp::{Filter, Rejection};

use db::models::LoginSession;

#[derive(Error, Debug)]
pub enum SignupPreConditionError {
    #[error("username not provided")]
    UsernameNotProvided,
    #[error("email not provided")]
    EmailNotProvided,
    #[error("password not provided")]
    PasswordNotProvided,
    #[error("email not valid")]
    EmailInvalid,
}
impl warp::reject::Reject for SignupPreConditionError {}

pub struct SignupDetails {
    pub username: String,
    pub email: String,
    pub password: String,
}

#[derive(Error, Debug)]
pub enum LoginPreConditionError {
    #[error("username or email not provided")]
    UsernameOrEmailNotProvided,
    #[error("password not provided")]
    PasswordNotProvided,
}
impl warp::reject::Reject for LoginPreConditionError {}

pub struct LoginDetails {
    pub username: Option<String>,
    pub email: Option<String>,
    pub password: String,
}

pub async fn signup(form_parameters: HashMap<String, String>) -> Result<SignupDetails, Rejection> {
    if form_parameters.get("username").is_none() {
        return Err(SignupPreConditionError::UsernameNotProvided.into());
    }
    if form_parameters.get("email").is_none() {
        return Err(SignupPreConditionError::EmailNotProvided.into());
    }
    if form_parameters.get("password").is_none() {
        return Err(SignupPreConditionError::PasswordNotProvided.into());
    }

    let username = form_parameters
        .get("username")
        .expect("username was None while parsing signup");
    let email = form_parameters
        .get("email")
        .expect("email was None while parsing signup");

    let email_re = Regex::new(r"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$")
        .expect("Couldn't build email parsing regex");
    if !email_re.is_match(email) {
        return Err(SignupPreConditionError::EmailInvalid.into());
    }

    let password = form_parameters
        .get("password")
        .expect("password was None while parsing signup");

    Ok(SignupDetails {
        username: username.clone(),
        email: email.clone(),
        password: password.clone(),
    })
}

pub async fn login(form_parameters: HashMap<String, String>) -> Result<LoginDetails, Rejection> {
    if form_parameters.get("username_or_email").is_none() {
        return Err(LoginPreConditionError::UsernameOrEmailNotProvided.into());
    }
    if form_parameters.get("password").is_none() {
        return Err(LoginPreConditionError::PasswordNotProvided.into());
    }

    let username_or_email = form_parameters
        .get("username_or_email")
        .expect("username was None while parsing login");

    let email_re = Regex::new(r"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$")
        .expect("Couldn't build email parsing regex");

    let password = form_parameters
        .get("password")
        .expect("password was None while parsing login");

    Ok(LoginDetails {
        username: if !email_re.is_match(&username_or_email) {
            Some(username_or_email.clone())
        } else {
            None
        },
        email: if email_re.is_match(&username_or_email) {
            Some(username_or_email.clone())
        } else {
            None
        },
        password: password.clone(),
    })
}

#[derive(Error, Debug)]
pub struct AuthenticatedUserError;
impl std::fmt::Display for AuthenticatedUserError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "Couldn't get user")
    }
}

impl warp::reject::Reject for AuthenticatedUserError {}

pub fn maybe_user(
    db_pool: Arc<Pool<ConnectionManager<PgConnection>>>,
) -> impl Filter<Extract = (Option<User>,), Error = Infallible> + Clone {
    warp::filters::cookie::optional("login_token").map(move |login_token: Option<String>| {
        let db_pool = db_pool.clone();

        if let Some(login_token) = login_token {
            let db_conn = db_pool.get();

            if let Ok(mut db_conn) = db_conn {
                let user_fetch = db::schema::login_sessions::table
                    .filter(db::schema::login_sessions::token.eq(&login_token))
                    .select(LoginSession::as_select())
                    .get_result(&mut db_conn)
                    .and_then(|session| {
                        db::schema::users::table
                            .filter(db::schema::users::id.eq(session.user_id))
                            .select(User::as_select())
                            .get_result(&mut db_conn)
                    });
                match user_fetch {
                    Ok(user) => Some(user),
                    Err(_err) => None,
                }
            } else {
                None
            }
        } else {
            None
        }
    })
}

pub fn user(
    db_pool: Arc<Pool<ConnectionManager<PgConnection>>>,
) -> impl Filter<Extract = (User,), Error = Rejection> + Clone {
    maybe_user(db_pool).and_then(move |maybe_user: Option<User>| async move {
        if let Some(user) = maybe_user {
            Ok::<User, Rejection>(user)
        } else {
            Err::<User, Rejection>(AuthenticatedUserError.into())
        }
    })
}
