use std::{fs::File, sync::Arc};
use warp::{Filter, Rejection};

pub fn extract_vapid_key_file(
    vapid_key_file: Arc<File>,
) -> impl Filter<Extract = (Arc<File>,), Error = Rejection> + Clone {
    warp::any().and_then(move || {
        let filter_vapid_key_file = vapid_key_file.clone();
        async move {
            // tokio::time::sleep(Duration::from_secs(2)).await;
            Ok::<Arc<File>, Rejection>(filter_vapid_key_file)
        }
    })
}
