use crate::init::mail::SmtpConfig;
use handlebars::Handlebars;
use init::RouteDependencies;
use std::{fs::File, net::SocketAddr, sync::Arc};
use tokio::runtime::Runtime;

mod filters;
mod init;
mod meta;
mod platform;
mod routes;
mod templates;

fn main() {
    let async_runtime = Runtime::new().expect("Could not create new async runtime!");

    let bind_address = std::env::var("BIND_ADDR").expect("No server BIND_ADDR provided!");

    let connection_string = std::env::var("DATABASE_URL").expect("DATABASE_URL envar not found");
    let pooled_pg_conn = init::connect_and_migrate_to_db(&connection_string);

    let mut hb = Handlebars::new();
    init::load_handlebars_templates(&mut hb).expect("Couldn't load Handlebars templates!");

    let routes = init::routes(RouteDependencies {
        hb: Arc::new(hb),
        db_conn: Arc::new(pooled_pg_conn),
        vapid_key_file: Arc::new(
            File::open("app/private-key.pem")
                .expect("Unable to load VAPID key file from private-key.pem"),
        ),
        smtp_config: Arc::new(SmtpConfig::init()),
    });
    let server = warp::serve(routes);

    async_runtime.block_on(async {
        server
            .run(
                bind_address
                    .parse::<SocketAddr>()
                    .expect("Could not parse socket address"),
            )
            .await;
    })
}
