use std::{future::Future, time::Duration};

pub async fn retry_async<T, E, Fut, F>(backoff_rounds: &[u32], mut f: F) -> Result<T, E>
where
    Fut: Future<Output = Result<T, E>>,
    F: FnMut() -> Fut,
{
    let mut num_retries = 0;
    let max_retries = backoff_rounds.len();

    let initial_result = f().await;
    if initial_result.is_err() {
        loop {
            tokio::time::sleep(Duration::from_millis(u64::from(
                backoff_rounds[num_retries],
            )))
            .await;

            let result = f().await;

            if result.is_ok() {
                break result;
            } else {
                if num_retries == max_retries - 1 {
                    break result;
                }

                num_retries += 1;
            }
        }
    } else {
        initial_result
    }
}

pub fn retry<T, E, F>(backoff_rounds: &[u32], mut f: F) -> Result<T, E>
where
    F: FnMut() -> Result<T, E>,
{
    let mut num_retries = 0;
    let max_retries = backoff_rounds.len();

    let initial_result = f();
    if initial_result.is_err() {
        loop {
            std::thread::sleep(Duration::from_millis(u64::from(
                backoff_rounds[num_retries],
            )));

            let result = f();

            if result.is_ok() {
                break result;
            } else {
                if num_retries == max_retries - 1 {
                    break result;
                }

                num_retries += 1;
            }
        }
    } else {
        initial_result
    }
}
