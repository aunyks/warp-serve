// @generated automatically by Diesel CLI.

diesel::table! {
    email_verification_requests (id) {
        id -> Int8,
        email_id -> Int8,
        code -> Varchar,
        created_at -> Timestamptz,
    }
}

diesel::table! {
    emails (id) {
        id -> Int8,
        user_id -> Int8,
        #[max_length = 50]
        email -> Varchar,
        is_verified -> Bool,
        is_primary -> Bool,
        added_at -> Timestamptz,
    }
}

diesel::table! {
    login_sessions (id) {
        id -> Int8,
        user_id -> Int8,
        token -> Varchar,
        created_at -> Timestamptz,
    }
}

diesel::table! {
    password_change_requests (id) {
        id -> Int8,
        user_id -> Int8,
        code -> Varchar,
        created_at -> Timestamptz,
    }
}

diesel::table! {
    users (id) {
        id -> Int8,
        #[max_length = 25]
        username -> Varchar,
        password -> Varchar,
        created_at -> Timestamptz,
    }
}

diesel::joinable!(email_verification_requests -> emails (email_id));
diesel::joinable!(emails -> users (user_id));
diesel::joinable!(login_sessions -> users (user_id));
diesel::joinable!(password_change_requests -> users (user_id));

diesel::allow_tables_to_appear_in_same_query!(
    email_verification_requests,
    emails,
    login_sessions,
    password_change_requests,
    users,
);
