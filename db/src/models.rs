use chrono::{DateTime, Utc};
use diesel::prelude::*;

#[derive(Insertable)]
#[diesel(table_name = crate::schema::users)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct NewUser {
    pub username: String,
    pub password: String,
}

#[derive(Queryable, Selectable)]
#[diesel(table_name = crate::schema::users)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct User {
    pub id: i64,
    pub username: String,
    pub password: String,
}

#[derive(Insertable)]
#[diesel(table_name = crate::schema::login_sessions)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct NewLoginSession {
    pub user_id: i64,
    pub token: String,
}

#[derive(Queryable, Selectable, Associations)]
#[diesel(belongs_to(User))]
#[diesel(table_name = crate::schema::login_sessions)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct LoginSession {
    pub id: i64,
    pub user_id: i64,
    pub token: String,
    pub created_at: DateTime<Utc>,
}

#[derive(Insertable)]
#[diesel(table_name = crate::schema::emails)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct NewEmail {
    pub user_id: i64,
    pub email: String,
    pub is_primary: bool,
}

#[derive(Queryable, Selectable, Associations)]
#[diesel(belongs_to(User))]
#[diesel(table_name = crate::schema::emails)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct Email {
    pub id: i64,
    pub user_id: i64,
    pub email: String,
    pub is_verified: bool,
}

#[derive(Queryable, Selectable, Associations)]
#[diesel(belongs_to(User))]
#[diesel(table_name = crate::schema::password_change_requests)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct PasswordChangeRequest {
    pub id: i64,
    pub user_id: i64,
    pub code: String,
    pub created_at: DateTime<Utc>,
}

#[derive(Insertable)]
#[diesel(table_name = crate::schema::email_verification_requests)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct NewEmailVerificationRequest {
    pub email_id: i64,
    pub code: String,
}

#[derive(Queryable, Selectable, Associations)]
#[diesel(belongs_to(Email))]
#[diesel(table_name = crate::schema::email_verification_requests)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct EmailVerificationRequest {
    pub id: i64,
    pub email_id: i64,
    pub code: String,
    pub created_at: DateTime<Utc>,
}
