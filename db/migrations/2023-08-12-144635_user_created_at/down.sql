ALTER TABLE users
DROP COLUMN created_at;

ALTER TABLE emails
DROP COLUMN added_at;