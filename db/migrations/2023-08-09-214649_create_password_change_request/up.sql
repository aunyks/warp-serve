CREATE TABLE password_change_request (
    id BIGSERIAL PRIMARY KEY,
    user_id BIGSERIAL REFERENCES users(id),
    code VARCHAR NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    UNIQUE(code)
);

CREATE TABLE email_verification_request (
    id BIGSERIAL PRIMARY KEY,
    email_id BIGSERIAL REFERENCES emails(id),
    code VARCHAR NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    UNIQUE(code)
);
