ALTER TABLE password_change_request
RENAME TO password_change_requests;

ALTER TABLE email_verification_request
RENAME TO email_verification_requests;
