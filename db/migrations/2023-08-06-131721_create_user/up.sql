CREATE TABLE users (
    id BIGSERIAL PRIMARY KEY,
    username VARCHAR NOT NULL,
    password VARCHAR NOT NULL,
    UNIQUE(username)
);

CREATE TABLE login_sessions (
    id BIGSERIAL PRIMARY KEY,
    user_id BIGSERIAL REFERENCES users(id),
    token VARCHAR NOT NULL,
    UNIQUE(token)
);

CREATE TABLE emails (
    id BIGSERIAL PRIMARY KEY,
    user_id BIGSERIAL REFERENCES users(id),
    email VARCHAR NOT NULL,
    UNIQUE(email)
);