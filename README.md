# warp-serve

## Start Entire Docker Environment
```sh
docker compose -p warp-serve -f docker/services.yml up -d
```

## Docker Build the App
```sh
ocker compose -p warp-serve -f docker/services.yml build app
```

## Docker Start the App
```sh
docker compose -p warp-serve -f docker/services.yml run -d --service-ports app
```

## Start App Locally
```sh
DATABASE_URL=postgres://pguser:test123@localhost/pgdb BIND_ADDR=127.0.0.1:80 cargo run -p app
```

## Generate VAPID Keys (Web Push)
**Generate private key file**
```sh
openssl ecparam -name prime256v1 -genkey -noout -out private-key.pem
```

**Generate public key Base64 from private key file**
```sh
openssl ec -in private-key.pem -pubout -outform DER|tail -c 65|base64|tr '/+' '_-'|tr -d '\n' > vapid-key.pub
```

## DB Migrations
**Install Diesel CLI**
```sh
cargo install diesel_cli
```

**Create a new migration**
```sh
cd db && diesel migration generate <migration_name>
```
Then edit up / down SQL migration files.

**Run migrations**
```sh
cd db && diesel migration run --database-url postgres://pguser:test123@localhost/pgdb
```

**Run migrations (PROD)**
Run migrations locally to update `db/src/schema.rs`. Deploying to prod with updated `db/src/schema.rs` and new migrations files will automatically run the migrations against the prod DB, no special commands needed.